# Shapeless Experimentation — this project can foo a bar! #

## Build & run ##

```sh
$ cd Shapeless-Experimentation
$ ./sbt
> run
```

## Contact ##

- Patrick Kaeding
- <a href="patrick@kaeding.name">patrick@kaeding.name</a>
