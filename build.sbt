/* basic project info */
name := "shapeless-example"

organization := "name.kaeding"

version := "0.1.0-SNAPSHOT"

description := "this project can foo a bar!"

//homepage := Some(url("https://github.com/pkaeding/myproj"))

startYear := Some(2013)

/* scala versions and options */
scalaVersion := "2.10.1"

crossScalaVersions := Seq("2.10.0", "2.10.1")

offline := false

scalacOptions ++= Seq("-deprecation", "-unchecked")

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

/* entry point */
mainClass in (Compile, packageBin) := Some("name.kaeding.shapelessexample.Main")

mainClass in (Compile, run) := Some("name.kaeding.shapelessexample.Main")

/* dependencies */
libraryDependencies ++= Seq (
  "com.chuusai" %% "shapeless" % "1.2.4"
)

/* you may need these repos */
resolvers ++= Seq(
  // Resolvers.sonatypeRepo("snapshots")
  // Resolvers.typesafeIvyRepo("snapshots")
  // Resolvers.typesafeIvyRepo("releases")
  // Resolvers.typesafeRepo("releases")
  // Resolvers.typesafeRepo("snapshots")
  // JavaNet2Repository,
  // JavaNet1Repository
)

/* sbt behavior */
logLevel in compile := Level.Warn

traceLevel := 5

releaseSettings

publishArtifact in Test := false

pomIncludeRepository := { _ => false }

pomExtra := (
  <developers>
    <developer>
      <id>pkaeding</id>
      <name>Patrick Kaeding</name>
      <email>patrick@kaeding.name</email>
      <!-- <url></url> -->
    </developer>
  </developers>
)

// Josh Suereth's step-by-step guide to publishing on sonatype
// httpcom://www.scala-sbt.org/using_sonatype.html

/* assembly plugin */
mainClass in AssemblyKeys.assembly := Some("name.kaeding.shapeless-example.Main")

assemblySettings

test in AssemblyKeys.assembly := {}
