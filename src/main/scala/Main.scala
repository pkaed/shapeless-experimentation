package name.kaeding.shapelessexample

import shapeless._, HList._
import TypeOperators._

class Apple(val color: String)
class Orange
class Pear

object Main extends App {
  object AppleEat {
    def eat(a: Apple) = s"eating ${a.color} Apple"
  }
  object OrangeEat {
    def eat(a: Orange) = "eating Orange"
  }
  object PearEat {
    def eat(a: Pear) = "eating Pear"
  }

  object doEat extends Poly1 {
    implicit val caseApple = at[Apple](AppleEat.eat(_))
    implicit val caseOrange = at[Orange](OrangeEat.eat(_))
    implicit val casePear = at[Pear](PearEat.eat(_))
  }

  println(doEat(new Apple("yellow")))

  val fruits = new Apple("red") :: new Orange :: new Apple("green") :: new Pear :: HNil

  val fruitStrings = fruits.map(doEat)
  println(fruitStrings)
}
